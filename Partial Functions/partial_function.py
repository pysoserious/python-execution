from functools import partial

def func(a,b,c,d):
    return a+b+c+d


g1 = partial(func,1,2,3)

print(g1(4))

g2 = partial(func, 1,2)

print(g2(3,4))