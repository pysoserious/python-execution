from sqlalchemy import create_engine, MetaData, Table
from sqlalchemy.orm import mapper, sessionmaker


class City(object):
    pass


engine = create_engine("postgres://postgres:postgres@localhost:5432/dvdrental", echo=True)
metadata = MetaData(engine)


def load_session():

    city_obj = Table('city', metadata, autoload = True)

    mapper(City, city_obj)

    session_obj = sessionmaker(bind=engine)
    session = session_obj()
    return session


if __name__ == "__main__":

    session = load_session()
    results = session.query(City).all()

    for row in results:
        print(row.city_id, end="   ")
        print(row.city, end="   ")
        print(row.country_id, end="  ")
        print(row.last_update)



