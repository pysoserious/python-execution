from sqlalchemy import Column, Integer, Table, String, DateTime, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


engine = create_engine("postgres://postgres:postgres@localhost:5432/dvdrental", echo=False)
dvdrental = declarative_base()
# https://github.com/davidfraser/sqlalchemy-orm-tutorial/

class City(dvdrental):

    __tablename__ = "city"

    city_id = Column('city_id', Integer, primary_key=True)
    city = Column('city', String)
    country_id = Column('country_id', Integer)
    last_update = Column('last_update', DateTime)

    def __init__(self, city_id, city, country_id, last_update):

        self.city_id = city_id
        self.city = city
        self.country_id = country_id
        self.last_update = last_update


def load_session():

    # metadata = dvdrental.metadata
    get_session = sessionmaker(bind=engine)
    session_obj= get_session()
    return session_obj


if __name__ == "__main__":

    session = load_session()
    result_set = session.query(City).all()

    for row in result_set:
        print(row.city_id, end="   ")
        print(row.city, end="   ")
        print(row.country_id, end="  ")
        print(row.last_update)