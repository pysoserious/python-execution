
from sqlalchemy import Integer, Column, String, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, Session

Base = declarative_base()


class User(Base):

    __tablename__ = "user"

    id = Column('id', Integer, primary_key=True)
    username = Column('username', String, unique=True)


engine = create_engine("sqlite:///:memory:", echo=True)

Base.metadata.create_all(bind=engine)


# to be executed inside this block.
session_maker = sessionmaker(bind=engine)


def add_new_record():
    session = session_maker()

    user = User()
    user.id = 0
    user.username = "ajinkya"

    session.add(user)
    session.commit()

    session.close()


def fetch_all_records():

    session = session_maker()

    users = session.query(User).all()

    if users is None:
        print("No records in DB")

    for u in users:
        print(u)

    session.close()


fetch_all_records()