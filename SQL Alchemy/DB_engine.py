
from sqlalchemy import create_engine, Column, ForeignKey, Integer, DateTime, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql import select

DvdRentalBase = declarative_base()


class City(DvdRentalBase):

    __tablename__ = "city"

    city_id = Column(Integer)
    city = Column(String(30))
    country_id = Column(Integer)
    last_update = Column(DateTime)
    
    def __str__(self):
        return self.city_id + " " + self.city + " " + self.country_id + " " + self.last_update


engine = create_engine('postgres+psycopg2://postgres:postgres@localhost:5432/dvdrental', echo=True)







