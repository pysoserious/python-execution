import numpy as np

list1 = [1.0,2.0,2.3]

npy_array = np.array(list1, dtype = np.float)

print("Array dimensions: {}".format(npy_array.ndim)) # shows the no of dimensions of array

print("Array shape: {}".format(npy_array.shape))


list2 =[[1,2,3],[4,5,6],[7,8,9]]

np_array = np.array(list2)

print(type(np_array))

print(np_array.shape, np_array.size, np_array.dtype, np_array.itemsize, np_array.itemsize*np_array.size)