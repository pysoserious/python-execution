import os
import sys
import random

def addNum(fnum, lnum):
    sumNum = fnum+lnum
    return sumNum

# print(sumNum) not defined, it is defined under function

print(addNum(2,3))

print("how to take input from conosle")

# name = sys.stdin.readline()

# print("Name : ", name)


# String manipulation

long_string = "this is a long string to play with"

print(long_string[0:4]) # -*- this prints a string betweenm 0 to 4 -*-

print(long_string[:-5])  # starts from length zero to length-5

print(long_string[-5:]) # start from length-5 to length