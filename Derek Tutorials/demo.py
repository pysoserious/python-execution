import random
import os
import sys


print("hello")

# single line comment

'''
multiline comments
'''

'''
Five main data types in python
Numbers 
Strings
Lists 
Tuples 
Dictionaries (Maps)
'''

name_1234 = 'ajinkya' # a variable name must start with alphabet and can have numbers and underscores
print(name_1234)

'''
Seven Algorithmic operators 
+, -, /, *, %, **(exponential) 
//(floor division, returns quotient and discard a remainder)
'''

print(" 5**2=", 5**2 )
print(" 5//2=", 5//2 )

# Order of operation to be remembered
#  multiplication and division happens first

print("1+2-3 * 4=", 1+2-3 * 4 / 2)


quote = "\"always remember this is a quote"
print(quote)

multi_line_quote = '''
this is a multi line
quote and it can be printed as..
'''

print(multi_line_quote)

print("%s %s %s" %('I like the quote', quote, multi_line_quote))

print('\n' * 5); # print 5 new lines

print("how to end new line ", end='|') # what character to be ended with
print('newline')


# lists

grocery_list =['juice', 'apple', 'avacado' ]
print("First Item = ", grocery_list[0])

grocery_list[0] = 'green apple juice'
print("First Item = ", grocery_list[0])


print(grocery_list[1:3])

list_one = [123, 234, 345]
list_two = ['one', 'two', 'three']

commbined_list = [list_one, list_two]

print(commbined_list)
print(commbined_list[1][1])

list_two.append('four')
print(commbined_list)

list_one.insert(0, 0)
print(commbined_list)

list_two.sort()
print(list_two)

list_two.reverse()
print(list_two)

del list_two[2]
print(list_two)
print(len(list_two))
print(max(list_two)) # incase of string it will print the string with max character
print(min(list_two))


#tuples are immutable lists

pi_tuple = 3,4,5,6,7

new_list = list(pi_tuple)
new_list.append(8)

new_tuple = tuple(new_list)

print(pi_tuple)
print(new_list)
print(new_tuple)


#dictionaries - like hash maps

myDictinary = {1:'one', 2 :'two'}

print(myDictinary)

print(myDictinary.keys())
print(myDictinary.values())
print(len(myDictinary))

print(myDictinary.get(1))
print(myDictinary[1]) # what is the difference ??

del myDictinary[1]

print(myDictinary)

age = 19

if age <=19 and age < 20 :
    print("if under age 19")
elif age == 19 or age > 20 :
    print("in elif")
else :
    print("else")

if not(age == 20) :
    print('age is not 20')

for x in range(0,10):
    print(x,'', end='')

print('\n')

for x in grocery_list :
    print(x)

for x in [0,1,2,3,4,5]:
    print(x)
print("=========================")
numList = [[0,1,2],[4,5,6],[7,8,9]]

for x in range(0,3):
    for y in range(0,3):
        print(numList[x][y])

randon_num = random.randrange(0,19)

print(randon_num)

while (randon_num != 15):
    print(randon_num)
    randon_num = random.randrange(0, 19)

i = 1

while (i <= 20):
    if(i%2 == 0):
        print(i)
    elif(i==9):
        break
    else:
        print("in else")
        i = i+1 # way to increment value
        continue
    i +=1 # way to increment value

