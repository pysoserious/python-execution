
def return_list(init, final):
    i = init
    while i <= final:
        yield i
        i+=1 # after returning from yield, execution resumes from this statement


for value in return_list(1,5):
    print(value)