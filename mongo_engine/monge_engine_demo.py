from mongoengine import *


class Metadata(EmbeddedDocument):
            tags = ListField(StringField())
            revisions = ListField(IntField())


class WikiPage(Document):
            title = StringField(required=True)
            text = StringField()
            metadata = EmbeddedDocumentField(Metadata)



connect('media_db')
page1 = WikiPage(title='Example 1', text='Wiki Page 1').save()