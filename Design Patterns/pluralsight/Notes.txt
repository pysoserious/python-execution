What is a Design Pattern?

-   Design pattern is a model solution to a common design problem.
    It describes a problem and a general approach to solving it.

Solid Principles of object oriented design

- Single Responsibility
- Open-Closed (open for inheritance but closed for modification)
- Liskov Substitution (sub-classes should be able to stand for their parent class without breaking anything)
- Interface segregation (many interfaces doing one this is better than one interface doing many things).
    We use abstract base classes and multiple inheritance in python.
- Dependency inversion (we should go for abstraction not implementations. Implementations may vary but abstractions should not)


Strategy pattern is also called as policy pattern