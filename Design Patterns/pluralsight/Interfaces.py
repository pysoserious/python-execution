import abc

class interface_a(object):

    # abstract base class ddefination
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def do_something(self):
        pass

    @abc.abstractclassmethod # deprecated in py3.3 instead use @classmethod @abc.abstractmethod
    def abs_method(self):
        pass

    @abc.abstractproperty # deprecated in python 3.3
    def some_property(self):
        pass

    @abc.get_cache_token
    def abs_getCacheToken(self):
        '''
            to register virtual subclasses
        '''
        pass