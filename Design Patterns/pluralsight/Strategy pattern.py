from abc import ABC, abstractmethod

class IStrategy(ABC):

    @abstractmethod
    def solve(self):
        pass


class Context(object):

    def __init__(self):
        self._strategyObj = None

    def strategy_solver(self, strategy_obj):
        self._strategyObj = strategy_obj
        strategy_obj.solve()


class LeapYearStrategy(IStrategy):

    def solve(self):
        print("Solve for leap year")


if __name__ == "__main__":
    context_obj = Context()

    leapYearStrategy_obj = LeapYearStrategy()

    context_obj.strategy_solver(leapYearStrategy_obj)

