def Singleton(class_name):

    instances = {}

    def get_instance(*args, **kwargs):
        if class_name not in instances:
            instances[class_name] = class_name(*args, **kwargs)
        return instances[class_name]
    return get_instance


@Singleton
class DB_connection(object):

    def __init__(self):
        self._connection_name = "sqllite3"

    def get_connecction_name(self):
        return self._connection_name

    def set_connection_name(self,connection_name):
        self._connection_name = connection_name


if __name__ == "__main__":

    obj1 = DB_connection()

    print(obj1.get_connecction_name())

    obj1.set_connection_name("mongodb")

    obj2 = DB_connection()

    print(obj2.get_connecction_name())