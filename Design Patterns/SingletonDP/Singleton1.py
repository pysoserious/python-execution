class MySingleton(object):

    _instance = None

    def __new__(self):
        if not self._instance:
            self._instance = super(MySingleton, self).__new__(self)
            self.name = "unkonwn"
        return self._instance


if __name__ == "__main__":
    x = MySingleton()
    print (x.name)

    x.name ="ajinkya"

    y = MySingleton()

    print(y.name)