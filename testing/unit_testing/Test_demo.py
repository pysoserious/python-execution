import unittest
from .fake_math import *


class TestDemo(unittest.TestCase): # test class name

    def setUp(self):  # called before each test
        print("Setup")
        pass

    def test_add(self):
        print('test add')

    def test_minus(self):
        print('test minus')

    def tearDown(self):
        print("TearDown")

    def shortDescription(self):
        print("short description           ")

    # def run(self, result=None):
      #   print("run")

    def debug(self):
        print("debug")


if __name__ == "__main__":

    unittest.main()
