import pytest
import sys

@pytest.mark.run_this_only
def test_passing():
    assert (1, 2, 3) == (1, 2, 3)


@pytest.mark.skip(reason = "I dont want to run this test")
def test_failing():
    assert (1, 2, 3) == (3, 2, 1)


@pytest.mark.skipif(sys.version_info > (3, 5), reason="I dont want to run this test")
def test_failing1():
    assert (1, 2, 3) == (3, 2, 1)


def test_substring():
    assert (1, 2, 3) == (1, 2, 3)