import pytest


'''
    Custom markers

'''


@pytest.mark.custom1
def test_t1():
    assert True


@pytest.mark.custom1
def test_t2():
    assert True


@pytest.mark.custom2
def test_t3():
    assert True


@pytest.mark.custom2
def test_t4():
    assert True
