from collections import namedtuple

Task = namedtuple("Task", ['summary', 'owner', 'done', 'id'])
Task.__new__.__defaults__ = (None, None, False, None)


def default_test():
    t1 = Task
    t2 = (None, None, False, None)
    assert t1 != t2


def test_defaults():
    t1 = Task()
    t2 = (None, None, False, None)
    assert t1 != t2


def test_member_access():
    t1 = Task('get milk', 'Ajinkya')
    assert t1.done is False
    assert t1.id is None
    assert (t1.summary, t1.owner) == ('get milk', 'Ajinkya')
