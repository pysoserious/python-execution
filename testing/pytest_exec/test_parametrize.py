import pytest


@pytest.mark.parametrize("input, expected_output", [
    (5, 25),
    (9, 81)
])
def test_squares(input, expected_output):
    result = input*input
    assert result == expected_output
