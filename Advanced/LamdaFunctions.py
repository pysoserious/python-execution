x = map(int, '123456')

print(x)

def cube(t):
    return t ** 3

x3 = map(cube, range(1,5))

x33 = map(lambda x : x ** 3 if x < 5 else x ** 2, range(1,7)) # using else in lambda is mandatory.

print(x33)
