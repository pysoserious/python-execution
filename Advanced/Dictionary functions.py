my_dict = {
    'fname' : 'Ajinkya',
    'lname' : 'Kharatkar',
    'education' : 'Graduate',
}

print(my_dict.items())

print(my_dict)

# print('has_keys()' , my_dict.has_key('education')) # only in py27

print ('popitem()' , my_dict.popitem()) # what does it pop. the last item.

print(my_dict)

my_dict_copy = my_dict.copy()

print(my_dict_copy)
print(id(my_dict_copy))
print(id(my_dict))

print(my_dict.values())

# fromKeys

print(my_dict.items())
print(my_dict.viewitems()) #diff
print(my_dict.viewkeys())

my_dict.setdefault('Country', 'India')

print(my_dict)

print(my_dict.get('Country'))

for k in my_dict:
    print(k)


iterItems_obj = my_dict.iteritems()

# iterItems_obj = my_dict.items() is available in python3
print(type(iterItems_obj))


for k in iterItems_obj:
    print(k)


# Emptys the dictionary. Dellocates the memory
my_dict.clear()

print(my_dict_copy)