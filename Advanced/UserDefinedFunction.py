

def function(): #function with return value
    print('statement1')
    print('statement2')
    return('return value')

print(function())


def functionNone(): #function without return value
    print('statement1')
    print('statement2')

print(functionNone())


def functionWithParam(a):
    print(a)

functionWithParam([1,2])   # such parameters are called non keyword parameters

functionWithParam(a=[1,2]) # keyword arguments


def functionWith2Params(a,b):
    print(a,b)

functionWith2Params(3,4)

functionWith2Params(a=4,b=3)

functionWith2Params(5,b=4)

# Rule 1
#functionWith2Params(7,a=5) # you should not have two values for a single argument in a function
# TypeError: functionWith2Params() got multiple values for keyword argument 'a'

# Rule 2
#functionWith2Params(a=5,6) # we cannot have a non keyword argument after keyword argument.

def functionWithDefaultValues(a, b=10):
    print a,b

functionWithDefaultValues(5)
functionWithDefaultValues(a=7)

# Rule 3
#def functionWithDefaultValues2(a,b=9,c): #Should not have non default argument after a default argument.
#    print a,b,c

def functionWithDefaultValues3(a, b, c=4):
    print a, b

# Call-by-Value and Call-by-Reference
#Two different objects with non-overlapping lifetimes may have the same id() value.


# Python uses pass by reference in case for passing parameters
varA = 34
print id(varA)
def expCallValue(a):
    print id(a)
    a = 90
    print id(a)

expCallValue(varA)
print id(varA)
print varA


# how to accept all args

def varArgs(*args): #args is a tuple and to be passed as non keyword arguments.
    print args


varArgs() #empty tuple
varArgs(1,2,3,4,5)

def varArgsWithMandatoryArgument(a, *args):
    print a, args

varArgsWithMandatoryArgument(2)
varArgsWithMandatoryArgument(1,2,3,5,6)


def varArgsWithKeyword(a, **kwargs): #kwargs is a dictionary
    print a, kwargs


def functionTest(a,b,c,*args, **kwargs):
    print a,b,c
    print args
    print kwargs
    return

functionTest(1,2,3,4,5,6, x=6,t=5)


#scope of variables

var = 10

def checkScope():
    var =20
    print var

    global var
    var = 80
    print var
    # avoid using global variables in functions unless mandatory.

checkScope()
print var


def checkScope(var):
    #global var # cannot be used
    var = 60
    print var

a =  (i for i in range(3,8))
a.next()
print type(a)
print a



# how to make a function a generator

#Generator in python

def myGenerator(low,high):
    while low < high:
        yield low
        low +=1

myGen = myGenerator(5,10) # defining our own xrange function.
for i in myGen:
    print i

# In python every function is an Object.
# If the fuction retuns a fuction it is called as closures.
# when a function accepts a function as  parameter is called as deccorators.
# closer is a func which returns a func as a object.

#closures in python

def dollar_rate(rate):
    def convert(quantity):
        print rate * quantity
    return convert

indianCurrency = dollar_rate(65)
dubaiCurrency = dollar_rate(3.67)
singaporeCurrency = dollar_rate(1.25)
malasianCurrency = dollar_rate(4)

print "indianCurrency ", indianCurrency(100)
print "dubaiCurrency ", dubaiCurrency(100)
print "singaporeCurrency ", singaporeCurrency(100)
print "malasianCurrency ", malasianCurrency(100)