

input = [1,2,3,4,5,6,7]

output_list = list(map(lambda x: x**2, input))

print(output_list)


def convert_req_to_obj(request_content):
    print(request_content)
    return request_content


def validate_req_object(request_object):
    print(request_object)


validate_req_object(convert_req_to_obj("reqobj"))