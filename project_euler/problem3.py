import math

def largest_prime(number):

    prime_list = []

    def is_prime(n):
        for j in range(int(math.sqrt(n))):
            if n % j:
                return False
            else:
                return True

    for i in range(number//2):
        if i % number == 0:
            if is_prime(i):
                prime_list.append(i)

    return prime_list.pop()


if __name__ == "__main__":
    print()
