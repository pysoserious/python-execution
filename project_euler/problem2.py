def fibonacci(number):
    a = 0
    b = 1
    c = int()
    list_of_even = []

    while c <= number:

        c = a + b
        if (c <= number):
            if c % 2 == 0:
                list_of_even.append(c)
                print(list_of_even)
            a = b
            b = c

    return sum(list_of_even)


if __name__ == '__main__':
    # input: Four million: 4000000
    print(fibonacci(4000000))