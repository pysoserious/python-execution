import timeit

def sum_of_multiple_3_5(num):

    t1 = timeit.default_timer()
    '''
        If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9.
        The sum of these multiples is 23.
        Find the sum of all the multiples of 3 or 5 below 1000.
    '''
    def sum_div_by(n):

        target = num-1
        p = target // n
        # print(p)
        s = (n * (p*(p+1))//2)
        return s

    return sum_div_by(3) + sum_div_by(5) - sum_div_by(15)


if __name__ == '__main__':

    print(sum_of_multiple_3_5(1000))
