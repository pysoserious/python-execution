import time

ticks = time.time()

print("Ticks from 1970 (epoch): ", ticks)

print(time.localtime(time.time()))
print(time.asctime(time.localtime(time.time())))