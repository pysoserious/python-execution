from enum import Enum

class Color (Enum):
    RED = 1
    GREEN = 2
    BLUE = 9
    # BLUE = 8 # TypeError: Attempted to reuse key: 'BLUE'
    CYAN = 9 # two enum members with same enum value is allowed.

if __name__ == "__main__":

    for colors in Color:
        print(colors)