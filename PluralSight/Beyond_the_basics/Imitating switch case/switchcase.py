
class FilterFactory(object):

    def get_shopping_filter(self):
        return "shopping filter"

    def get_booking_filter(self):
        return "booking filter"

    @staticmethod
    def return_filter_obj(header):

        filter_switcher = {

            "Admin_Shopping" :  FilterFactory.get_shopping_filter,
            "Admin_booking" :   FilterFactory.get_booking_filter,
        }

        try:
            # return filter_switcher.get(header, "No such filter exist")
            return filter_switcher.get(header)
        except KeyError:
            return None
        finally:
            print("it is always executed")


if __name__ == "__main__":

    filter_name = FilterFactory.return_filter_obj(header="agvadf")

    print(filter_name)