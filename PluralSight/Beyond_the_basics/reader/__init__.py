from reader.reader import Reader as reader2
from reader.reader import Reader as reader1

# from . import reader.reader.Reader -  not possible.

# you cannot import logical entity as - > import reader.reader.Reader


__all__ = ['reader1', 'reader2']