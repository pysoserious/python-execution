import urllib
import urllib.request


print(type(urllib))  # <class 'module'>

print(type(urllib.request))  # <class 'module'>

# but urllib is a package and urllib.request is module which can be identified as -

print(urllib.__path__)  # ['C:\\Miniconda3\\pkgs\\python-3.6.3-h52e1c9f_4\\lib\\urllib']

print(urllib.request.__path__)  # gives error

"""
    Summary - A package and module are represented with type module but a package as __path__ attribute. 
"""