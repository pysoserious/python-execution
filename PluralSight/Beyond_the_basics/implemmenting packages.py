import urllib
from urllib import request
from urllib.response import addbase as rr

# A package has a __file__ attribute which returns its path
print(urllib.__file__)
print(urllib.__path__)

# returns request.py
print(request.__file__)
print(request.__path__) # error it is not a package, hence no __path__

# returns error as addbase logical entity is not logical
#print(rr.__file__)

# print(rr.__path__)
