'''
What is context manager?

A object which is used with a WITH statement is context manager
It ensures that a resource is properly and automatically managed.
eg-
with context-manager:
    body

context-manager protocol

__enter__(self)
    1. Called before the execution of with statement
    2. return value is bound to as variable
    3. can return value of any type
    4. commonly returns contextmanager itself.

__exit__(self, exc_type, exc_val, exc_tb)
    1. called when body with body exists.
    2. Responsible for cleaning up resources used by context manager.

    exc_type = exception type
    exc_val = exception object
    exc_tb exception traceback

How to stop propagating  exception traceback outside with block?

'''

class Context_Manager():

    def __init__(self):
        pass

    def __enter__(self):
        print("In context-manager __enter__")

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type is None:
            print("Exit without an exception")
        else:
            print("In context-manager __exit__ with {},{},{}".format(exc_tb,exc_type,exc_val))
        return True
        # does not propagate error outside with block when True or returns a object


if __name__ == "__main__":

    with Context_Manager() as x:
        print("Context manager with block")
        raise ValueError("exception raise for testing")

    f = Context_Manager()
    print(f)
    with f as g:
        print("is f == g", f is g) # why this returns false

    file = open("dummy.txt", 'r')
    print(file)
    with file as file_obj:
        print(file is file_obj)