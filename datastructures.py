employee1 = ['ajinkya', 30000, 'cybage']
employee2 = ['abhishek', 31000, 'symantec']

ListofList = [employee1, employee2]

print(ListofList)

dictOfLists = {1:employee1, 2:employee2}

print(dictOfLists)

empDict1 = {'name':'ajinkya', 'salary':30000, 'company':'cybage'}
empDict2 = {'name':'abhishek', 'salary':31000, 'company':'symantec'}

listOfDict = [empDict1,empDict2]

print(listOfDict)

dictOfDict = {'emp1':empDict1, 'emp2': empDict2}

print(dictOfDict)

print(type(empDict2) == type(empDict1))

print("id of none ", id(None))

noneList = None

print("id of none list", id(noneList))

nullList = []

print("id of empty list", id(nullList))

courses = ('python')   # tuple having single value is considered as a single string

print(type(courses))

courses = ('python', )

print(type(courses))

# set

employees = {'ajinkya', 'manju', 'snhehal', 'mani'}

print(employees.__len__())

print(employees)

managers ={'rishu', 'minu', 'kahsish' , 'jatin'}

print(managers ^ employees)







