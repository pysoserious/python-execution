import re

'''
WAP to find out the line number which contains a word python in a normal txt a file.
'''


fileobj = open("Infile.txt", 'r')

reg_pattern = re.compile("python", re.I )

match= list()

for lines in fileobj.readlines():
    match.append(re.findall(reg_pattern, lines))


print(match)



'''
WAP a program to accept password from a user and validate 
1. atleast one character
2. atleast one special character
3. atleast one digit
4. password length sould be greater than 8
'''



def validate_password(password):
    if(len(password) > 8):
        pattern_capital = re.compile('.[A-Z]')
        pattern_lower = re.compile('.[a-z]')
        pattern_digits = re.compile('.[0-9]')
        pattern_sym = re.compile('.[!@#$%^&*]')
        if(re.match(pattern_capital, password) and
               re.match(pattern_lower, password) and
               re.match(pattern_digits, password) and
               re.match(pattern_sym, password)):
            return True
        return False
    else:
        print('''
            The password must contain:
            1. atleast one character
            2. atleast one special character
            3. atleast one digit
            4. password length sould be greater than 8
        ''')
        return False

print(validate_password("Ajinkya08#"))

'''
WAP a program to read a log file and find the count of ip address available in a log file.
'''

def get_all_ip_address():
    log_file = open('LogFile.log')

    reg_pattern = re.compile('')

    match = list()

    for lines in fileobj.readlines():
        match.append(re.findall(reg_pattern, lines))

    print(match)
