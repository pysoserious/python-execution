class BinaryNode(object):

    def __init__(self, value):

        self.left = None
        self.right = None
        self.value = value

    @property
    def left(self):
        return self.left

    @property.setter
    def left(self, left_node):
        self.left = left_node

    @property
    def right(self):
        return self.right

    @property.setter
    def right(self, right_node):
        self.right = right_node