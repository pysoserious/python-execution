
class BinaryNode(object):

    def __init__(self, value):

        self.left = None
        self.right = None
        self.value = value


class BinaryTree(object):

    def __init__(self):
        self.root = None

    def attach_data(self, data, node=None):

        # if root is None
        if self.root is None:
            self.root = BinaryNode(data)
            print("root added")

        # if root is not None
        else:
            # check data to add is less than current node value - GO LEFT
            if data < node.value:
                if node.left is None:
                    node.left = BinaryNode(data)
                    print("data added to left")
                # else traverse till child node
                else:
                    self.attach_data(data, node.left)
            # else GO RIGHT
            else:
                if node.right is None:
                    node.right = BinaryNode(data)
                    print("data added to right")
                else: # else traverse till child node
                    self.attach_data(data, node.right)

    def print_tree(self, node):

        if node:
            print(node.value, end="   ")
            self.print_tree(node.left)
            self.print_tree(node.right)



if __name__ == "__main__":

    my_tree = BinaryTree()
    print(my_tree.root)

    my_tree.attach_data(12, my_tree.root)
    my_tree.attach_data(11, my_tree.root)
    my_tree.attach_data(15, my_tree.root)

    my_tree.print_tree(my_tree.root)

