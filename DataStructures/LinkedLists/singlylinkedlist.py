
class SinglyLinkedList(object):

    class Node:

        def __init__(self, v, n=None):
            self.value = v
            self._next = n

    def __init__(self):
        self.head = None
        self.size = 0

    def is_empty(self):
        return self.size == 0

    def length(self):
        return self.size


    def insert_at_end(self, value):

        # create a new node that is to be inserted
        new_node = self.Node(value)

        # point at the start of the singly linked list i.e. head
        current = self.head

        # this returns a SinglyLinkList object
        # curr = self

        # if head is null then add first node to the head
        if self.head is None:
            self.head = new_node

        # Traverse till last node whose next is None i.e. does not point to any other node.
        else:
            while current.next is not None:
                current = current.next

            # Attach node to the last node or to the node whose NEXT is None
            current.next = new_node

        # increment the size of the linked list
        self.size = +1


if __name__ == "__main__":

    Sll = SinglyLinkedList()

    Sll.insert_at_end("firtnode")

    print(Sll.is_empty())