

input_int_list = [1, 2, 3, 4, 5, 6, 7, 8, 9]
input_alphabets = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', ]

input_words = ["Ajinkya", "Abhijeet", "Abhishek", "Prakash", "Sunil"]

def binary_search(input_list_param, element):

    midpoint = len(input_list_param) // 2

    if len(input_list_param) != 0:
        if input_list_param[midpoint] == element:
            return True
        else:
            if element < input_list_param[midpoint]:
                return binary_search(input_list_param[:midpoint], element)
            else:
                return binary_search(input_list_param[midpoint + 1:], element)
    return False


if __name__ == "__main__":
    print("Found 3: ", binary_search(input_int_list, 3))
    print("Found j: ", binary_search(input_alphabets, 'j'))
    print("Found 'Prakash': ", binary_search(input_words, "Prakash"))



