

def string_reverse(a):

    if type(a) != str:
        return None
    elif len(a) == 1:
        return a
    else:

        leftSubStr = a[:int(len(a)/2)]
        rightSubStr = a[int((len(a)/2)):]
        return string_reverse(rightSubStr) + string_reverse(leftSubStr)


print(string_reverse(90))
print(string_reverse("ajinkya"))

string = "Ajinkya"

print(string[::-1])

length = int(len(string)/2)
print(length)
print(string[:length])

print(list(reversed(string)))
