import sqlite3

try:
    db = sqlite3.connect('Employee')
    # if schema is not available then it creates the schema automatically.
    cursor = db.cursor()
    # cursor.execute(''' create table emp(id int, name text, dept text)''')
    # cursor.execute(''' insert into emp values ('1', 'ajinkya', 'comp_sci')''')
    print(cursor.execute('''select * from emp'''))

except Exception as E:
    print("unable to interact with db")
else:
    # db.commit()
    print(cursor.fetchall())
    print("table created the successfully")
    # cursor.execute('''drop table emp''')
    db.close()
