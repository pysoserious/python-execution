import sqlite3

id = input("id: ")
name = input("name: ")
dept = input("dept: ")
salary = input("salary: ")



try:
    db = sqlite3.connect('Employee')
    # if schema is not available then it creates the schema automatically.
    cursor = db.cursor()

    # cursor.execute(''' create table emp(id int, name text, dept text)''')


    # old style of formatting
    # cursor.execute(''' insert into emp values (%r, %r, %r, %r )''' %(id, name, dept, salary))

    # new style in formatting
    cursor.execute(''' insert into emp values ('{0}' , '{1}', '{2}', '{3}' )''' .format(id, name, dept, salary))

    # cursor.execute('''select * from emp''')

except Exception as E:
    print("unable to interact with db")
else:
    db.commit()
    print(cursor.fetchall())
    print("table created the successfully")
    # cursor.execute('''drop table emp''')
    db.close()
