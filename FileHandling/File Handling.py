infile = open('file.txt', 'r')

print infile.read(5)

print infile.readline()

print infile.tell()

print infile.readlines()

print infile.seek(0,0)    # for second argument

'''
    for second argument in seek.
    0 is for starting position.
    1 is for current location.
    2 is for end of file.
'''

print infile.read(3)






