class Solution:
    def twoSum(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        l_output_index = []
        b_index_found = False

        for i in range(0, len(nums)):
            second_ele = target - nums[i]
            if (second_ele) in nums:
                l_output_index.append(i)
                l_output_index.append(nums.index(second_ele))
                return l_output_index



if __name__ == "__main__":

    '''
    case 1: nums has +ve values, target is +ve
    case 2: nums has -ve values, target is +ve
    case 3: nums has +ve values, target is -ve
    case 4: nums has -ve values, target is -ve
    '''

    soln_obj = Solution()
    print(soln_obj.twoSum([2,3,4,5,6,6], -7))
