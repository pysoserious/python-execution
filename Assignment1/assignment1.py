import math
import time

# WAP to swap the value of two variables in Python without using third variable.

num1 = 10
num2 = 20

num1 = (num1 + num2) #30
num2 = num1 - num2
num1 = num1 - num2

print("num1 : %d and num2: %d",(num1, num2))

# WAP to find the first hundred prime numbers.

t1 = time.time()
for x in range(1,100,2):
    for y in range(3,int(math.sqrt(x))+1,2):
        if x % y == 0:
            break
    else:
        print "prime number: ", x
t2 = time.time()
print t2-t1

# WAP to create a Fibonacci series of n numbers.

num1 = 0
num2 = 1
print num1
print num2
sum = 0
while sum <= 100:
    sum = num1 + num2
    num1 = num2
    num2 = sum
    if sum <=100:
        print sum

# WAP to get the factorial of user defined number.

'''input_number = int(raw_input("Enter a number to get factorial: "))
fact = 1
while(input_number >=1):
    fact = fact * input_number
    input_number -= 1
print fact'''

# WAP to check whether the user input number is Prime, Armstrong number and find out the ascii value of that number.

'''input_num = raw_input("Enter a number to check prime or armstrong: ")
armstrong_num = 0

for i in input_num:
    armstrong_num = int(i) ** input_num.__len__() + armstrong_num

if armstrong_num == int(input_num):
    print(input_num, "is a armstrong number")

for y in range(3,int(math.sqrt(int(input_num)))+1,2):
        if int(input_num) % y == 0:
            break
else:
    print "prime number: ", input_num'''

# WAP to convert temperature from degree centigrade to Fahrenheit.
degree = float(raw_input("Enter to convert into Fahrenheit: "))

Fahrenheit = degree * 9/5 + 32

print "Fahrenheit: ", Fahrenheit



orig_num = 8802750827

orig_str = str(orig_num)

print orig_str[::-1]


# 1) WAP to swap the value of two variables in Python without using third variable. 
def swapTwoNum(num1, num2):
    num1 = (num1 + num2) #30
    num2 = num1 - num2
    num1 = num1 - num2

# 2) WAP to create a DS of students (define DS as per the protocols) and print the sum of scores in five subjects and gets the each student percentage. 
# 3) WAP to convert temperature from degree centigrade to Fahrenheit. Ex: (°C × 9/5) + 32 = °F 
# 4) WAP to get the basic salary from employee and calculate it gross salary (Basic salary + 10% DA and 12%TA) 
# 5) WAP to reverse a given number. 
# 6) WAP to create a Fibonacci series of n numbers. 
# 7) WAP to find the distinct, duplicate and unique elements in a list. 
# 8) WAP to get the factorial of user defined number. 
# 9) WAP to find the first hundred prime numbers. 
# 10)WAP to accept three inputs from the user and find the greatest number among 3 numbers. (Get three numbers from raw_input) 
# 11)WAP to check whether the user input number is Prime, Armstrong number and find out the ascii value of that number. 
# 12)WAP to get the count of words in the statement string and the count of vowels in complete statement. 
# 13)WAP to Sort the list using lambda function mylist = [["john", 1, "a"], ["larry", 0, "b"]]. Sort the list by seconditem 1 and 0. 
# 14)WAP to find out the leap year. (Except a year from the user using raw_input) 
# 15)WAP to reverse/inverse key:value like below.  dict1 = {͚a͛: 1, 'b':2} Expected result: dict2 = {1: 'a', 2: ͚b͛} 