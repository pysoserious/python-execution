def parent(num):

    def inner1():
        return("This is inner1")

    def inner2():
        return("This is inner2")

    if num < 10:
        # return inner1() # nonetype object is not callable
        return inner1
    else:
        # return inner1() # nonetype object is not callable
        return inner2



if __name__ == "__main__":

    fun1 = parent(10)
    fun2 = parent(40)

    print(fun1) # printing firstclass object of fun1
    print(fun2) # printing firstclass object of fun2

    print(fun1())
    print(fun2())

