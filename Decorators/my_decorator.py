

def my_decorator(function_argument):
    def wrapper():
        function_argument()
    return wrapper


@my_decorator
def this_is_same_function():
    print("same_function")


if __name__ == "__main__":
    this_is_same_function()