def outer_outerfunctions():

    def inner1():
        print("This is inner1 function")

    def inner2():
        print("This is inner2 function")

    inner1()
    inner2()

outer_outerfunctions()


# inner1() calling inner function will give you error.