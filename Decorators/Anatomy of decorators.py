
def my_decorator(some_function):

    def wrapper():

        print("Some functionality before call")

        some_function()

        print("Some functionality after call")

    return wrapper


def other_some_function():
    print("in Other_some_function")


just_some_function  =  my_decorator(other_some_function)

just_some_function()