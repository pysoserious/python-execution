import pdb
pdb.set_trace()


'''
Python Debugger manual program
'''

print('program started')

var = 10

def func(a,b):
    print('Inside funtion')
    return a+b;

for i in range(2):
    print('inside loop')
    print(i)

var2 = 20
c = func(2,3)

print('program completed')
