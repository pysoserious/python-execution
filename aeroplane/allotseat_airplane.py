class Seat():

    def __init__(self, seat_type='-C-'):
        self.seat_type = seat_type
        self.passenger_no = None
        self.is_seat_alloc = False


class Block():

    '''
    This class determines the dimensions of seats blocks in airplane
    '''
    def __init__(self, cols, rows, block_type='C'):
        self.block = [[Seat() for x in range(cols)] for y in range(rows)]
        self.block_type = block_type
        self.cols = cols
        self.rows = rows

        for rows in self.block:
            if cols is 1:
                rows[0].seat_type = '-A-'
                continue
            else:
                if self.block_type is 'L':
                    rows[0].seat_type = '-W-'
                    rows[cols - 1].seat_type = '-A-'
                elif self.block_type is 'R':
                    rows[0].seat_type = '-A-'
                    rows[cols - 1].seat_type = '-W-'
                elif self.block_type is 'LR':
                    rows[0].seat_type = '-W-'
                    rows[cols - 1].seat_type = '-W-'
                else:
                    rows[0].seat_type = '-A-'
                    rows[cols - 1].seat_type = '-A-'

    def append_empty_seats(self, max_rows):
        if max_rows > self.rows:
            rows_to_append = max_rows - self.rows
            for r in range(rows_to_append):
                self.block.append([Seat(seat_type='---') for x in range(self.cols)])


class AirplaneSeats():

    '''
    This class is composition of block and seats.
    Also the member functions perform to allot passenger seats.
    '''

    def __init__(self, no_of_passengers, list_of_blocks, c_size, r_size):
        self.no_of_passengers = no_of_passengers
        self.list_of_blocks = list_of_blocks
        self.c_size = c_size
        self.r_size = r_size
        self.passenger_counter = 1

        self.block_cols = []
        for b in range(len(self.list_of_blocks)):
            if b is not 0:
                self.block_cols.append(self.list_of_blocks[b].cols + self.block_cols[b - 1])
            else:
                self.block_cols.append(self.list_of_blocks[b].cols)

        for b in self.list_of_blocks:
            b.append_empty_seats(self.r_size)


    def passenger_allocation(self):

        '''
        This member performs the seat allocation to passenger
        :return:None
        '''
        def assign_passenger(seat, seat_type):
            if seat.seat_type == seat_type and seat.is_seat_alloc is False and self.passenger_counter <= self.no_of_passengers:
                seat.passenger_no = self.passenger_counter
                seat.is_seat_alloc = True
                self.passenger_counter += 1
            return seat

        allotment_order = ['-A-','-W-','-C-']
        for seat_type in allotment_order:
            for r in range(self.r_size):
                row_list = [blocks.block[r] for blocks in self.list_of_blocks]
                for seats in row_list:
                    for seat in seats:
                        assign_passenger(seat,seat_type)

    def print_airplane_seats(self):
        '''
        This Member functions
        :return:None
        '''

        for r in range(self.r_size):
            row_list = [blocks.block[r] for blocks in self.list_of_blocks]
            for i, seats in enumerate(row_list):
                for seat in seats:
                    if seat.is_seat_alloc:
                        print('{num:03d}'.format(num=seat.passenger_no), end='  ')
                    else:
                        print(seat.seat_type, end='  ')
                if i is not (len(row_list) - 1):
                    print(' | ', end=' ')
            print('')


def main(dimension_list, no_passenger):
    '''
    :param dimension_list: It is an array of dimensions of each block of seats in airplane
    :param no_passenger: no of passengers to be allot as per the rule in the seats of airplane
    :return: None
    '''

    block_list = []
    airplane_r_size = 0
    airplane_c_size = 0
    no_of_blocks = len(dimension_list)
    for dl, dimensions in enumerate(dimension_list):
        airplane_c_size += dimensions[1]
        if airplane_r_size < dimensions[0]:
            airplane_r_size = dimensions[0]
        if dl is 0:
            block_list.append(Block(dimensions[1], dimensions[0], block_type='L'))
        elif dl is no_of_blocks - 1:
            block_list.append(Block(dimensions[1], dimensions[0], block_type='R'))
        else:
            block_list.append(Block(dimensions[1], dimensions[0]))

    airplane_obj = AirplaneSeats(no_passenger, block_list, airplane_c_size, airplane_r_size)

    airplane_obj.passenger_allocation()

    airplane_obj.print_airplane_seats()


if __name__ == '__main__':

    dimensions_array = []
    print("Enter space separated dimensions as rows and columns")
    while True:
        block_input = input()
        dimension = []
        if len(block_input) is 3 and ' ' in block_input:
            rows, cols = block_input.split(' ')
            dimension.append(int(rows))
            dimension.append(int(cols))
            dimensions_array.append(dimension)
        else:
            no_of_passengers = int(block_input)
            break

    main(dimensions_array, no_of_passengers)
    # main([[3, 4], [4, 5], [2, 6]], 25)






