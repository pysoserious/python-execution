

class Seat():

    def __init__(self, seat_type='-C-'):
        self.seat_type = seat_type
        self.passenger_no = None
        self.is_seat_alloc = False


class Block():

    def __init__(self, cols, rows, block_type = 'C'):
        self.block = [[Seat() for x in range(cols)] for y in range(rows)]
        self.block_type = block_type
        self.cols = cols
        self.rows = rows

        for rows in self.block:
            if cols is 1:
                rows[0].seat_type = '-A-'
                continue
            else:
                if self.block_type is 'L':
                    rows[0].seat_type = '-W-'
                elif self.block_type is 'R':
                    rows[cols - 1].seat_type = '-W-'
                elif self.block_type is 'LR':
                    rows[0].seat_type = '-W-'
                    rows[cols - 1].seat_type = '-W-'
                else:
                    rows[0].seat_type = '-A-'
                    rows[cols - 1].seat_type = '-A-'

    def append_empty_seats(self, max_rows):

        if max_rows > self.rows:

            rows_to_append = max_rows - self.rows
            for r in range(rows_to_append):
                self.block = [Seat(seat_type='---') for x in range(cols)]


class AirplaneSeats():

    def __init__(self, no_of_passengers, list_of_blocks, c_size, r_size):
        self.no_of_passengers = no_of_passengers
        self.list_of_blocks = list_of_blocks
        self.c_size = c_size
        self.r_size = r_size
        self.airplane_map = [[Seat() for x in range(c_size)] for y in range(r_size)]

        self.block_cols = []
        for b in range(len(self.list_of_blocks)):
            if b is not 0:
                self.block_cols.append(self.list_of_blocks[b].cols + self.block_cols[b - 1])
            else:
                self.block_cols.append(self.list_of_blocks[b].cols)

    def passenger_allocation(self):
        pass



if __name__ == "__main__":

    no_of_passengers = int(input("p: "))
    blocks = []
    airplane_r_size = 0
    airplane_c_size = 0
    while True:
        block_size = input('b: ')
        if len(block_size) is 3 and ' ' in block_size:
            cols , rows = block_size.split(' ')
            cols = int(cols)
            rows = int(rows)
            airplane_c_size = airplane_c_size + cols
            if airplane_r_size < rows:
                airplane_r_size = rows
            if len(blocks) is 0:
                blocks.append(Block(cols, rows, 'L'))
            elif len(blocks) is 3:
                blocks.append(Block(cols, rows, 'R'))
            else:
                blocks.append(Block(cols, rows, 'C'))
        else:
            break

    a = AirplaneSeats(no_of_passengers, blocks, airplane_c_size, airplane_r_size)


    row_count = 0
    for rows in a.airplane_map:
        for seat in rows:
            # print(seat.seat_type, end=' ')
            row_count += 1
            if row_count in a.block_cols and row_count is not a.block_cols[len(a.block_cols)-1]:
                # print("|", end=' ')
                pass
        row_count = 0
        # print('')

    # for blocks in a.list_of_blocks:
    #     seats = blocks.block
    #     for s in seats:
    #         print(s)

    for r in range(a.r_size):
        for b in a.list_of_blocks:
            if b.block[r]:
                for c in b.block[r]:
                    print(c.seat_type, end=' ')
                print("")











