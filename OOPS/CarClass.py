class Car(object):
    # class attribute
    __no_of_tyres = 4  # private is made by adding '__'
    steering_type = "manual"

    def __init__(self, name, no_of_tyres = 5, steering_type ='manual'):
        self.name = name
        self.__no_of_tyres = no_of_tyres
        self.steering_type=steering_type


    def move_car(self, direction): # self is a special variable which holds a reference of a instance.
        self.director = direction
        print("car_moved", direction)

    def __del__(self):
        print ('Destroying an object of Car class i.e {}'.format(self.name))


if __name__ == '__main__':

    audi = Car('audi') # creating an object/instance of the class car
    mere = Car('mere') # creating new object


    # creating a attribute of a object externally

    # called as instance attribute.
    audi.x = "external attribute"

    print(audi.x)

    print(Car.steering_type) # static attribute because called via class name.

    audi.move_car(direction="north")

