from CarClass import Car


class EuroCar(Car):
    def moveCar(self, direction):
        self.direction = direction
        print('EuroCar is moving towards', self.direction)
        super(EuroCar, self).moveCar(direction)


class AmericanCar(Car):
    def moveCar(self, direction):
        self.direction = direction
        print('AmericanCar is moving towards', self.direction)
        super(AmericanCar, self).moveCar(direction)


class IndianCar(EuroCar, AmericanCar):
    pass


audi = IndianCar()
audi.moveCar(direction='left')