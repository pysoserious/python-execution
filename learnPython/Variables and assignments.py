

# we can initialse multiple variables in a single line
a, b = 3, 4

print(a, b)

# This will not work!
one = 1
two = 2
hello = "hello"

#print(one + two + hello)

# Various variable assignments
mystring = "hello"
myfloat = 10.0
myint = 20

# testing code
if mystring == "hello":
    print("String: %s" % mystring)
if isinstance(myfloat, float) and myfloat == 10.0:
    print("Float: %f" % myfloat)
if isinstance(myint, int) and myint == 20:
    print("Integer: %d" % myint)


