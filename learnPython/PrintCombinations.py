'''

https://www.linuxnix.com/python-print-statement-examples/

The print statement is useful for joining multiple words, strings, numbers with different data types as well. Below examples shows on how to join multiple strings to form a single sentence. Print statement has operators which are useful for joining same data types, different data types and formatting output in a meaningful way. Below table will show different operators and formatting options available.

Operator	Usage
          ,	 Used to print multiple strings in a line.
          +	 Used to concatenate two strings into single string.
        %	 Used to concatenate Strings & Integers

'''

# Print command with,(comma) operator

print('hi','my','name','is','ajinkya')  #comma acts as space and concatinates string.

print('I','am', 24 ,'years','old')

print(1,2,3,4,5,6,7,8,9,0)

