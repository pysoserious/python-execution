import datetime

list_= [1,2,3,4]

string_obj = "sample string"

print("Repr of list_ :", repr(list_))
print("Str f list_ :", str(list))

print(repr(string_obj))
print(str(string_obj))

# the goal of repr is two be unambiguous
# the goal of str is to be str

datetime_obj = datetime.datetime.now()
datetime_str = str(datetime_obj)


print(repr(datetime_obj))
print(str(datetime_obj))
print(str(datetime_str))