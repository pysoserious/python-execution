import json


with open('json_sample.json') as f:
    data = json.load(f)

    for servlet in data['web-app']['servlet']:
        del servlet['servlet-class']

    # dumps() loads data
    json_data = json.dumps(data, indent=3)
    print(json_data)


# write modified json to file.
with open("output_json.json", "w") as op_file:

    json.dump(data, op_file, indent=3)
