import json

json_string = '''
{
    "people":[
        {
            "name": "Ajinkya",
            "phone": "9096494273",
            "e-mail": ["ajinkya.pk@gmail.com", "mail@ajinkyaprakash.com"],
            "has_licence": "True"        
        },
        {
            "name": "Kharatkar",
            "phone": "9028094273",
            "e-mail": null,
            "has_licence": "false"
        }
    ]
}
'''

data = json.loads(json_string)

print(data)
print("Type of json object: ",type(data))
print("Type of people key in json : ",type(data['people']))

for getName in data['people']:
    print(getName['name'])


for phone in data['people']:
    del phone['phone']

new_string = json.dumps(data, indent=2, sort_keys=True)

print(new_string)