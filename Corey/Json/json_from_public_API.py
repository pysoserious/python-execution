import json
from urllib.request import urlopen

with urlopen('https://jsonplaceholder.typicode.com/posts/1') as response:
    
    json_source = response.read()
    
data = json.loads(json_source)

print(json.dumps(data, indent=3))