
x = "Global_x"


def outer():
    x = "outer_x"

    def inner():
        x = "inner_x"

        def in_inner():
            nonlocal x
            nonlocal x # writing second time doest not count it will only check for variable in very outer scope
            x = "in_inner_x"
            print(x)

        in_inner()
        print(x)

    inner()
    print(x)


outer()
print(x)
