class Employee:

    appraisal = 1.05
    num_of_employee = 0

    def __init__(self, first, last, salary):
        self.first = first
        self.last = last
        self.email = first + '.' + last + '@comapany.com'
        self.salary = salary
        Employee.num_of_employee += 1

    def _object(self):
        return '{} {} {} {}'.format(self.first, self.last, self.email, self.salary)

    def apply_raise(self):
        self.salary = int(self.salary * self.appraisal)

    def set_default_raise(self, appraisal):
        Employee.appraisal = appraisal

    #private method
    def __private_method(self):
        print("This is a private method of class")

    #private static method
    @staticmethod
    def __prv_static_method():
        print("Private static method")

    @classmethod
    def class_method(cls):
        #self.first -  cannot use self.
        print("in class method")


if __name__ == '__main__':

    emp1 = Employee('Ajinkya', 'Kharatkar', 7000)
    emp2 = Employee('Abhijeet', 'Wanya', 8000)

    print(emp1._object()) # what is the difference between the two method calls
    print(Employee._object(emp1))

    print(emp1.__dict__) #class variable 'appraisal' is not included in obect namespace
    print(Employee.__dict__)

    emp1.appraisal = 1.06 # appraisal variable is added to the namespace when modified through a instance
    print(emp1.__dict__)

    emp1.apply_raise()

    print(emp1.salary)

    # emp1.__private_method()  # if we try to call private method outside class it gives error as Employee instance has no attribute '__private_method'

    # hack to access private method out any class. But should not be used in real time programming.
    print(dir(Employee))

    # Employee._Employee__private_method() # unbound method __private_method() must be called with Employee instance as first argument

    emp1._Employee__private_method()

    emp1._Employee__prv_static_method() # private static method is also called through a instance on class

    Employee._Employee__prv_static_method() # private static method can also be called through class name.


    emp1.class_method()
    Employee.class_method()
