from abc import ABC, abstractclassmethod
import logging
import abc

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

class AbstractClass34(ABC):

    @abstractclassmethod
    def abstract_method(self):
        pass

    @staticmethod
    @abstractclassmethod
    def static_abs_method():
        pass

    @abc.abstractmethod
    @abstractclassmethod
    def class_abs_method(cls):
        pass

    @static_abs_method
    def concrete_method(cls, self):
        logger.info("This is concrete method in abstract class")

'''
# Python 3.0+
from abc import ABCMeta, abstractmethod
class Abstract(metaclass=ABCMeta):
    @abstractmethod
    def foo(self):
        pass
# Python 2
from abc import ABCMeta, abstractmethod
class Abstract:
    __metaclass__ = ABCMeta

    @abstractmethod
    def foo(self):
        pass
'''

class Abs_Implementation(AbstractClass34):

    def abstract_method(self):
        logger.info("abs")

    @staticmethod
    def static_abs_method():
        pass

    @classmethod
    def class_abs_method(cls):
        pass

if __name__ == "__main__":

    # abs_obj = AbstractClass34() # error in abstract method.

    abs1 = Abs_Implementation()
    abs1.abstract_method()