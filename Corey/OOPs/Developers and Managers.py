from Corey.OOPs.BasicEmployeeClass import Employee
# in python 2  -- from BasicEmployeeClass import Employee


class Developer(Employee):
    appraisal = 1.07

    def __init__(self, first, last, pay, prog_lang= "OOPS"):
        super().__init__(first, last, pay)
        self.prog_lang = prog_lang

    def _object(self):
        return '{} {} {} {}'.format(self.first, self.last, self.email, self.salary, self.prog_lang)


if __name__ == "__main__":

    dev1 = Developer("ajinkya", "Kharatkar",  9000, "python")
    dev2 = Developer("Deepti", "Kharatkar", 8000, "java")

    print(dev1.salary)
    print(dev2.num_of_employee)

    print(help(Developer))


    print(isinstance(Developer, Employee))
