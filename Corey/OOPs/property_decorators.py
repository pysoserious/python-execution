class Employee:

    appraisal = 1.05
    num_of_employee = 0

    def __init__(self, first, last, salary):
        self.first = first
        self.last = last
        self.salary = salary
        Employee.num_of_employee += 1

    @property
    def fullname(self):
        return "{firstname} {lastname}".format(firstname=self.first, lastname=self.last)

    @property
    def email(self):
        return "{}.{}@company.com".format(self.first,self.last)

    @fullname.setter  # mandatory to have first declared fullname as a property
    def fullname(self, name):
        self.first, self.last = name.split(' ')

    @fullname.deleter
    def fullname(self):
        print("fullname deleted")
        self.first = None
        self.last = None


if __name__ == "__main__":

    emp1 = Employee("ajinkya", "kharatkar" , 6789)

    emp1.first = "Deepti"

    print(emp1.email)

    emp1.fullname = "Anita Kharatkar"

    print(emp1.fullname)

    del emp1.fullname

