'''
LEGB rule
Local, Enclosing, Global, Built-in

Local => variables enclosed in a function.
Enclosing => enclosed in a block.
Global => Defined at module level or declared by global keyword.
Built-in => built-in python variables
'''

x = "global_x"


def func1():
    y = "local_y"
    # print(y)
    print(x)


func1()
# print(y) # NameError: name 'y' is not defined
print(x)


def func2():
    x = "local_x"
    print(x)

func2()
print(x)


def func3():
    global x
    x = "global changed to local x"
    print(x)


func3()
print(x)

# how to define a global variable within a funtion


def func4():
    global global_var
    global_var = "global variable def in function"
    print(global_var)


func4()
print(global_var)


def func5(z): # z acts as a local variable and cannot be accessed outside of the function
    print(z)


func5('local z')
# print(z) # NameError: name 'z' is not defined

# builtin scope


def min():
    pass

#m = min([1,2,3,4,5]) # it will try to find the function in the local scope and then in builtin scope.
#print(m)




