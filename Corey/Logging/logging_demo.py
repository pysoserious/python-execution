import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s:%(funcName)s:%(levelname)s:%(message)s')
filehandler = logging.FileHandler('logging.log')
filehandler.setFormatter(formatter)
logger.addHandler(filehandler)

def logging_demo():
    logger.info("this will not print")
    logger.debug("this will not be shown on console.")
    logger.warning("this will be shown on console")


# DEBUG: detailed information, typically fo interest when diagnosing problems.

# INFO: Confirming that things are working as expected

# WARNING: An indication that something unexpected happened, problems coming in near future. (level: 1)
# when logging level set to 1 it will ignore DEBUG and INFO. It will log ERROR and CRITICAL

# ERROR: due to some problem, software has not able to perform some function.

# CRITICAL: critical issue and software may not continue to work properly.

# default logging level is 1 i.e. warning

# https://docs.python.org/3/library/logging.html


if __name__ == "__main__":

    #logging.basicConfig(filename="logfile.log", level=logging.DEBUG,
    #                   format='%(asctime)s:%(funcName)s:%(levelname)s:%(message)s')
    logging_demo()