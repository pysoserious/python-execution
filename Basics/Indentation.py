
obj = (0,)

if len(obj) > 1:
    print ("length of an object ibn greater than 1")
elif len(obj) == 1:
    print("length of the object is equal to 1")
else:
    pass
    #print ("enter any implementation")
print("program completed")


object = list(range(10))

print(object)

if len(object) > 1:
    print("length of the object is greater than 1")
    if len(object) == 10:
        print("Length of the object is 10")
        print("program completed")
    else:
        print("Length is less than 10")
elif len(object) == 1:
    print("Length of the object is less than one")
else:
    print("object is empty")
print("program completed")


# WAP to get a number from a user and print 10 odd numbers after that number


input_num = int(input())
i = 0
while i <=10:
    input_num += 1
    if input_num % 2 != 0:
        print("Odd numbers: ", input_num)
        i += 1
else:
    print("else of while loop")
print("program completed")


'''
    debug is easy with else with while
    else will not be called if while exits with a break statement.
    
    used for cleanup of resources when while has existed successfully.
'''

obj = "string"

for var in "aeiou":
    if var == i and (len(obj) == 5 or len(obj) > 5):
        break
    print(var)
else:
    print("Elements by elements output")

list_obj =  {'course1': "python", 'course2': "java", 'course1': "aws"}

for var in list_obj:
    print(var)
else:
    print("elements by output")



