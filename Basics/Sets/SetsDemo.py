'''
Sets are lists with no duplicate entries. Let's say you want to collect a list of words used in a paragraph:
'''

print(set("my name is Eric and Eric is my name".split()))

#To find out which members attended both events, you may use the "intersection" method:

a = set(["Jake", "John", "Eric"])
b = set(["John", "Jill"])

print(a.intersection(b))
print(b.intersection(a))

#To find out which members attended only one of the events, use the "symmetric_difference" method:

print(a.symmetric_difference(b))
print(b.symmetric_difference(a))

