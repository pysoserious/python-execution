print(type('default string '))
print(type(b'string with b '))

'''
Output in Python 2.x (Bytes is same as str)
<type 'str'>
<type 'str'>

Output in Python 3.x (Bytes and str are different)
<class 'str'>
<class 'bytes'>
'''

print(type('default string '))
print(type(u'string with b '))

'''
Output in Python 2.x (Unicode and str are different)
<type 'str'>
<type 'unicode'>

Output in Python 3.x (Unicode and str are same)
<class 'str'>
<class 'str'>
'''